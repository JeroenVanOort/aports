# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ada
pkgver=2.4.0
pkgrel=0
pkgdesc="WHATWG-compliant and fast URL parser written in modern C++"
url="https://ada-url.github.io/ada"
arch="all"
license="( Apache-2.0 OR MIT ) AND MPL-2.0"
makedepends="
	cmake
	cxxopts-dev
	gtest-dev
	samurai
	"
checkdepends="simdjson-dev"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/ada-url/ada/archive/v$pkgver/ada-$pkgver.tar.gz
	use-system-simdjson-cxxopts.patch
	"

build() {
	local crossopts=
	[ "$CBUILD" != "$CHOST" ] && crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"

	local shared; for shared in OFF ON; do
		cmake -G Ninja -B build \
			-DCMAKE_BUILD_TYPE=MinSizeRel \
			-DCMAKE_INSTALL_PREFIX=/usr \
			-DCMAKE_INSTALL_LIBDIR=lib \
			-DBUILD_SHARED_LIBS=$shared \
			-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
			-DADA_BENCHMARKS=OFF \
			$crossopts
		cmake --build build
	done
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -m644 build/src/libada.a -t "$pkgdir"/usr/lib/
}

sha512sums="
e92eada454530c47c7a9bc12f4c9edd23ab2a2ec41d93304ff7da3e3176dc0a3e0f2dfc1f692769bd6fade5407dc59f152b38e8717a99e33781b596afa88ad73  ada-2.4.0.tar.gz
3574006234403db9330a682c7486a178b2b9681eba5c053c84eac2e4706f1942aee6f532a78a6e81312326901f0f81ad3520ddd29bf7327223c2e30044b20694  use-system-simdjson-cxxopts.patch
"
