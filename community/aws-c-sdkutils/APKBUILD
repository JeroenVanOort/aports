# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-sdkutils
pkgver=0.1.10
pkgrel=0
pkgdesc="C99 library implementing AWS SDK specific utilities"
url="https://github.com/awslabs/aws-c-sdkutils"
# s390x: aws-c-common
arch="all !s390x"
license="Apache-2.0"
makedepends="
	aws-c-common-dev
	cmake
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-sdkutils/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja\
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-sdkutils
}

sha512sums="
6a427a81cf4188417e47be3d1d6a2e0bc380987cfb7cbfe2c94a831a22e8c936733e25a4f1384e23f7106d3b526a2640e4f620c76f545dcbb1eb884ad1b07769  aws-c-sdkutils-0.1.10.tar.gz
"
