# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dragon
pkgver=23.04.1
pkgrel=0
pkgdesc="A multimedia player where the focus is on simplicity, instead of features"
url="https://kde.org/applications/multimedia/org.kde.dragonplayer"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-only OR GPL-3.0-only"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dragon-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
20fcbefadb07c8d8e01125b531b90565c4209c23d85aac755179852f6e952c04fe271d13c893693ea860aae22b1273512021e434b87b28b8b9e84a20266772de  dragon-23.04.1.tar.xz
"
