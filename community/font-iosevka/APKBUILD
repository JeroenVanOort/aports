# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=22.1.2
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
d1a11b6b55e16e64f2dd3f7a113465c5b4fc945f0dbebb486587c9fa0600ac013a32c001f097af78e95a8a977419eb4022b0503dc54079a2185e5c90b1ded925  super-ttc-iosevka-22.1.2.zip
ff20798fc768c175bcf57ab35354ffe66ee4fb9a605803392b518df340f13b6e2ba4704861b2e23fd77870db910179231ed7187f56f051e03736eaf4065f880f  super-ttc-iosevka-aile-22.1.2.zip
3bc115f4169cc74a6ce0410bb1fc2ce3cd1920befba3fb4a74f96e8b22ce04238da4aa1dc0d37870e2703e53553cc018e80cd91a67fac81ff1750e580d2a0710  super-ttc-iosevka-slab-22.1.2.zip
e0b47246d14244cf2de80b1314b66fd6f63fa7f42495820b1fdfb377076910ff5783a2854542485669d154c61d34feb7fbd09d18264491f78f3ecdf5f8e5301b  super-ttc-iosevka-curly-22.1.2.zip
d8df333e283efbfde554eed2011cb2f9f667c26ea89aba0c806d0bfa1a11285bfc6416e33b177d00494e604e5b54f096cd3b8c5268826726f11c90fcc94954c3  super-ttc-iosevka-curly-slab-22.1.2.zip
"
