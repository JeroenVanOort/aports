# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcalendarcore
pkgver=5.106.0
pkgrel=0
pkgdesc="The KDE calendar access library"
options="!check" # RecursOn-ConnectDaily(2|3|6) make the builders stuck
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev libical-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

replaces="kcalcore"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# testrecurtodo, testreadrecurrenceid, testicaltimezones, testmemorycalendar and testtimesininterval are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "test(recurtodo|readrecurrenceid|icaltimezones|memorycalendar|timesininterval)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
df3d5111b4aa5261dd692faaf33e082119550a9328a9f01c3ae71f41e6054c1753c3d14afe5346cc7b0ced8dcae069812e96801bf123b5b4ef15bb9327b582bc  kcalendarcore-5.106.0.tar.xz
"
