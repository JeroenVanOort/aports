# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kclock
pkgver=23.04.1
pkgrel=0
pkgdesc="Clock app for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/kclock"
# armhf blocked by qt5-qtdeclarative
# x86 broken
arch="all !armhf !x86"
license="LicenseRef-KDE-Accepted-GPL"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kclock-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e06339ea940639731bcc7347981e8e55828527fc9f26326984d631d1db064554b9c7712a7a36c315f8ec1a92e9017e4b9dcc483713cc36cf3613ec99bc76188d  kclock-23.04.1.tar.xz
"
