# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-dev-utils
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/"
pkgdesc="Small utilities for developers using KDE/Qt libs/frameworks"
license="(LGPL-2.0-only OR LGPL-3.0-only) AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-utils-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
32b72af1e40c8d2bc74b09aea4f4616d6e4f02de8c6ec425a7719a9e290b0ef7aeafa5a5600155a0dd6b20b901ca3582d1647c41978e11b6e586ef35bff35b26  kde-dev-utils-23.04.1.tar.xz
"
