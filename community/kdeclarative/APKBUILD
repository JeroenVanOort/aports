# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeclarative
pkgver=5.106.0
pkgrel=0
pkgdesc="Provides integration of QML and KDE Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libepoxy-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# quickviewsharedengine requires OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "quickviewsharedengine"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9faed6c524de26929f22df20f9cdfa611f91560892bdcbe4748dd7debd55f84f4b48682904d92b9a7c582a3a4db302073e2c84e832744dc296e55fc7a3badefe  kdeclarative-5.106.0.tar.xz
"
