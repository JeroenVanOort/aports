# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdnssd
pkgver=5.106.0
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Network service discovery using Zeroconf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends="avahi"
depends_dev="
	avahi-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdnssd-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d4970486354c4a2705557b568458072195f3cc825cd9f2e18908132237540198c9153922ffc3bb90615df7eecd4f4bb3f61249df492ddc066a4632bed8c9cc7e  kdnssd-5.106.0.tar.xz
"
