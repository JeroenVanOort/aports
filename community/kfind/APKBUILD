# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kfind
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/utilities/kfind"
pkgdesc="Find Files/Folders"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfind-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7caececbefcf7b0ab23c535ad8b4736db62744d22a3e287be6433ed7da9b8fb12b9a59c57bf5ca9bceb491018b3abab9a3264c93f4016ab77a1274fae53d749a  kfind-23.04.1.tar.xz
"
