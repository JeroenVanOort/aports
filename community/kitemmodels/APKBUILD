# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitemmodels
pkgver=5.106.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemmodels-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kdescendantsproxymodel(_smoketest|test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1244d941fbbafc52cbcb5a8e4ec4c345284ec991518e30e72a299991ade3f3d8403a22d0da73fc1693df8073e07cdf4879c5318bff4be25b5b0ed4370bfda69e  kitemmodels-5.106.0.tar.xz
"
