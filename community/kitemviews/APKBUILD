# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitemviews
pkgver=5.106.0
pkgrel=0
pkgdesc="Widget addons for Qt Model/View"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only"
depends_dev="qt5-qtbase-dev"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemviews-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
27d4734943413fe61a9cfd631bf931d7d49b25fcd6c92c3f65b94b1b1cdcaf5fbd56ad63b70ba0ada77edcc1705094a542ea6a8eed16e27849ede9d828e0f065  kitemviews-5.106.0.tar.xz
"
