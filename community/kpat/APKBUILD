# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpat
pkgver=23.04.1
pkgrel=0
pkgdesc="KPatience offers a selection of solitaire card games"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kpat/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	black-hole-solver-dev
	extra-cmake-modules
	freecell-solver-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
# Broken tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
537b6754bf72ebf3c034dfbfbef77687aaeead95332cdb4e5d00a5d205ae7559f4cc3074147055d603f598a27c950f117520032bcfdfb25674f89d6d80f49bed  kpat-23.04.1.tar.xz
"
