# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kruler
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/graphics/org.kde.kruler"
pkgdesc="An on-screen ruler for measuring pixels"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kruler-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bfac79be0abc227389f6bea75888601a58e1de2772e3b65a822dcfff885c2cbd4af8aa2ceed46285e190606d3222cb027b085c38efb493866ae1f73e80eb8c95  kruler-23.04.1.tar.xz
"
