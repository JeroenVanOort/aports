# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktexteditor
pkgver=5.106.0
pkgrel=0
pkgdesc="Advanced embeddable text editor"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kauth-dev
	kconfig-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	sonnet-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
options="!check" # fail on builders
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktexteditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2a4e7fd7d879febf9e3eb78acad9a3a77f6106335884dad83a1c4f3cba642f8677c2550550e42b8e1531440b0fc7652a64a0d46c480e0fb75ddcf26976b89943  ktexteditor-5.106.0.tar.xz
"
