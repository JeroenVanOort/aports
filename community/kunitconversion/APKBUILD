# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kunitconversion
pkgver=5.106.0
pkgrel=0
pkgdesc="Support for unit conversion"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kunitconversion-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -j1
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
6a1d4c8369ec06473466a37d42850f01b96db3adbfd836f77cfec255152407ea44a2373dec70654ba39ee23829759a8cc73c034657b84fe1463432cc0bc876a2  kunitconversion-5.106.0.tar.xz
"
