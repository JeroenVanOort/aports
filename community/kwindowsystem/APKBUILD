# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwindowsystem
pkgver=5.106.0
pkgrel=0
pkgdesc="Access to the windowing system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT AND (LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen libxrender-dev xcb-util-keysyms-dev xcb-util-wm-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwindowsystem-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kwindowsystem-kwindowinfox11test hangs
	# kwindowsystem-kwindowsystemx11test, kwindowsystem-kwindowsystem_threadtest and kwindowsystem-netrootinfotestwm are broken
	# kwindowsystem-netwininfotestwm is broken on s390x
	local skipped_tests="kwindowsystem-("
	local tests="
		kwindowinfox11test
		kwindowsystemx11test
		kwindowsystem_threadtest
		netrootinfotestwm
		"
	case "$CARCH" in
		s390x) tests="$tests
			netwininfotestwm
			"
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
d41f0e9d7c82bf2870ad05501744d4b02f527e3114eba46826a390c5ab242f66ee7d7e69657b2fc3dd23c051eed16baacd849874ca676969e6bd11fe9b15eb50  kwindowsystem-5.106.0.tar.xz
"
