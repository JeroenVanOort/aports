# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailcommon
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64" # Limited by messagelib -> qt5-qtwebengine
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
# TODO: Consider replacing gnupg with specific gnupg subpackages that mailcommon really needs.
depends="gnupg"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmailtransport-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	mailimporter-dev
	messagelib-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	syntax-highlighting-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5664aa157cddc532a98c9cb3831afd9532177fc07733c23dc96efdbbf37f7bc979dc1d121575fa023a5f9b6b7e2c6e3367d4bb34045f2c4fed8b0e174ea1c8e7  mailcommon-23.04.1.tar.xz
"
