# Maintainer: crapStone <crapstone01@gmail.com>
pkgname=mdbook
pkgver=0.4.29
pkgrel=0
pkgdesc="mdBook is a utility to create modern online books from Markdown files"
url="https://rust-lang.github.io/mdBook/"
arch="all"
license="MPL-2.0"
makedepends="rust cargo"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rust-lang/mdBook/archive/v$pkgver.tar.gz"
builddir="$srcdir/mdBook-$pkgver"
[ "$CARCH" = "riscv64" ] && options="$options textrels"
export CARGO_HOME="$srcdir"/cargo

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release

	./target/release/mdbook completions bash > $pkgname.bash
	./target/release/mdbook completions fish > $pkgname.fish
	./target/release/mdbook completions zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mdbook "$pkgdir"/usr/bin/mdbook

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
3917f212fe1070216824d9df69b3b110c1d74f7901bb7f35a7153759d60fd187c0b588cf90c389e5d6080d6011ebb227f25e11c1a9ed41a3126d7f8bc274d1e7  mdbook-0.4.29.tar.gz
"
