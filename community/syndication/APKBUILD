# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=syndication
pkgver=5.106.0
pkgrel=0
pkgdesc="An RSS/Atom parser library"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND BSD-3-Clause"
depends_dev="qt5-qtbase-dev kcodecs-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syndication-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
267063495b0afbb9c50d48f38c67f4bb60799c14f1c889a9663c0a21f61763eec4927f8b5ca7f88b8d71877b090c213c6648a15a092b541bda03812690a45f55  syndication-5.106.0.tar.xz
"
