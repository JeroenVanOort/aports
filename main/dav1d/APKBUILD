# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dav1d
pkgver=1.2.0
pkgrel=0
pkgdesc="small and fast AV1 Decoder"
url="https://code.videolan.org/videolan/dav1d"
arch="all"
license="BSD-2-Clause"
makedepends="nasm meson"
subpackages="$pkgname-dev lib$pkgname:libs"
source="https://code.videolan.org/videolan/dav1d/-/archive/$pkgver/dav1d-$pkgver.tar.bz2"

build() {
	case "$CARCH" in
		armhf*) meson_opts="-Denable_asm=false" ;;
		*) meson_opts="-Denable_asm=true" ;;
	esac

	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Denable_tests=true \
		-Denable_tools=true \
		-Dfuzzing_engine=none \
		-Dtestdata_tests=false \
		$meson_opts \
		build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

check() {
	meson test --no-rebuild --print-errorlogs -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

sha512sums="
7e0968a938c890df311a88e02212ab6154daf40d34f2c9761f75819209d5284dbc7ac0560e464db5da3af579d0b2c0ec78b659e6311b72992ddcfdd3d584d3b6  dav1d-1.2.0.tar.bz2
"
