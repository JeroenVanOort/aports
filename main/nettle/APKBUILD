# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=nettle
pkgver=3.9
pkgrel=1
pkgdesc="Low-level cryptographic library"
url="https://www.lysator.liu.se/~nisse/nettle/"
arch="all"
license="GPL-2.0-or-later OR LGPL-3.0-or-later"
depends_dev="gmp-dev"
makedepends="$depends_dev m4"
subpackages="$pkgname-static $pkgname-dev $pkgname-utils"
source="https://ftp.gnu.org/gnu/nettle/nettle-$pkgver.tar.gz"

# secfixes:
#   3.7.3-r0:
#     - CVE-2021-3580
#   3.7.2-r0:
#     - CVE-2021-20305

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libdir=/usr/lib \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-fat \
		--enable-shared \
		--disable-openssl \
		--enable-static
	make
	# strip comments in fields from .pc as it confuses pkgconf
	sed -i -e 's/ \#.*//' ./*.pc
}

check() {
	make -C examples  # required for rsa-encrypt
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utils for nettle"
	amove usr/bin
}

sha512sums="
5e44f59b37ec1e92345fce0b963151d1f2aabf01b3a197b8d931067c51af4ba025059c6a07f2bcd19b17eb49d6ede98f5c200e58d340959826cda473459d2fba  nettle-3.9.tar.gz
"
