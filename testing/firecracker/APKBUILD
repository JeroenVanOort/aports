# Contributor: Dennis Przytarski <dennis@przytarski.com>
# Maintainer: Dennis Przytarski <dennis@przytarski.com>
pkgname=firecracker
pkgver=1.3.2
pkgrel=0
pkgdesc="Secure and fast microVMs for serverless computing"
url="https://github.com/firecracker-microvm/firecracker"
arch="aarch64 x86_64"
license="Apache-2.0"
makedepends="rust cargo clang-dev linux-headers"
subpackages="
	$pkgname-seccompiler
	$pkgname-rebase-snap:rebase_snap
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/firecracker-microvm/firecracker/archive/v$pkgver.tar.gz
	userfaultfd.patch
	"

prepare() {
	default_prepare

	cargo fetch --locked --target "$CTARGET"

	# Change the seccomp filters' names so they get embedded into the binary
	for a in $arch; do
		mv -v resources/seccomp/"$a-unknown-linux-musl.json" resources/seccomp/"$a-alpine-linux-musl.json"
	done
}

build() {
	export CARGO_PROFILE_RELEASE_OPT_LEVEL=2
	cargo build \
		--package firecracker \
		--package jailer \
		--package seccompiler \
		--package rebase-snap \
		--target "$CTARGET" \
		--all-features \
		--frozen \
		--release
}

check() {
	# jailer: tests failed testing functionality of cgroups
	# seccompiler: tests failed spawning a thread
	cargo test \
		--package firecracker \
		--package rebase-snap \
		--target "$CTARGET" \
		--frozen

	# Other integration tests need pytest and docker
}

package() {
	install -Dm755 build/cargo_target/$CTARGET/release/firecracker \
		-t "$pkgdir"/usr/bin
	install -Dm755 build/cargo_target/$CTARGET/release/jailer \
		-t "$pkgdir"/usr/bin
	install -Dm755 build/cargo_target/$CTARGET/release/rebase-snap \
		-t "$pkgdir"/usr/bin
	install -Dm755 build/cargo_target/$CTARGET/release/seccompiler-bin \
		-t "$pkgdir"/usr/bin
}

seccompiler() {
	pkgdesc="$pkgdesc - seccompiler"

	amove usr/bin/seccompiler-bin
}

rebase_snap() {
	pkgdesc="$pkgdesc - rebasing diff snapshot tool"

	amove usr/bin/rebase-snap
}

sha512sums="
9c5e4d1fc4e6ce0eacd13f552545824114e2723465b7bc501a33a98f7b4d5efd4f078f1dc064a96f1e70847a8a19316d6e16dc317c2ce51695c251a9293a83c5  firecracker-1.3.2.tar.gz
921fa0b124504fa382c1566f8acaa5360401d1adbe138d32e1915c2aa7ed83341d9c992eefd5b5250fded70b8748274e5b838e166370795e602caf51958cf9e5  userfaultfd.patch
"
